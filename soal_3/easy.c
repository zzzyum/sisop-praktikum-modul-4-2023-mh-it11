#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <dirent.h>
#include <errno.h>
#include <libgen.h>
#include <pthread.h>
#include <sys/stat.h>
#include <sys/time.h>

#define FUSE_USE_VERSION 28
#include <fuse.h>

#define CHUNK_SIZE 1024

static const char *dirpath = "/home/yumz/main";
static const char *modPrefix = "module_";

static void singleDescLog(char *level, char *cmd, char *desc)
{
    char timestamp[20];

    FILE *logFile = fopen("/home/yumz/fs_module.log", "a");

    time_t t = time(NULL);
    struct tm *tm_info = localtime(&t);

    strftime(timestamp, sizeof(timestamp), "%y%m%d-%H:%M:%S", tm_info);

    fprintf(logFile, "%s::%s::%s::%s\n", level, timestamp, cmd, desc);

    fclose(logFile);
}

static void doubleDescLog(char *level, char *cmd, char *firstDesc, char *secondDesc)
{
    char timestamp[20];

    FILE *logFile = fopen("/home/yumz/fs_module.log", "a");

    time_t t = time(NULL);
    struct tm *tm_info = localtime(&t);

    strftime(timestamp, sizeof(timestamp), "%y%m%d-%H:%M:%S", tm_info);

    fprintf(logFile, "%s::%s::%s::%s::%s\n", level, timestamp, cmd, firstDesc, secondDesc);

    fclose(logFile);
}

static void modularDir(const char *dirPath)
{
    DIR *dir = opendir(dirPath);
    if (dir == NULL) {
        perror("Error opening directory for modularization");
        return;
    }

    struct dirent *entry;
    while ((entry = readdir(dir)) != NULL) {
        if (entry->d_type == DT_REG) {
            char filePath[1024];
            sprintf(filePath, "%s/%s", dirPath, entry->d_name);

            struct stat st;
            if (stat(filePath, &st) == -1) {
                perror("Error getting file information");
                continue;
            }

            if (st.st_size > CHUNK_SIZE) {
                FILE *originalFile = fopen(filePath, "rb");
                if (originalFile == NULL) {
                    perror("Error opening file for modularization");
                    continue;
                }

                int chunkIndex = 0;
                while (!feof(originalFile)) {
                    char chunkPath[1030];
                    sprintf(chunkPath, "%s.%03d", filePath, chunkIndex);

                    FILE *chunkFile = fopen(chunkPath, "wb");
                    if (chunkFile == NULL) {
                        perror("Error creating chunk file");
                        fclose(originalFile);
                        break;
                    }

                    char buffer[CHUNK_SIZE];
                    size_t bytesRead = fread(buffer, 1, CHUNK_SIZE, originalFile);
                    fwrite(buffer, 1, bytesRead, chunkFile);

                    fclose(chunkFile);
                    chunkIndex++;
                }

                fclose(originalFile);

                if (remove(filePath) != 0) {
                    perror("Error deleting original file");
                }
            }
        }
    }

    closedir(dir);
}

static int compareToSort(const void *a, const void *b) 
{ 
    struct dirent **dirA = (struct dirent **)a;
    struct dirent **dirB = (struct dirent **)b;

    return strcmp((*dirA)->d_name, (*dirB)->d_name); 
} 

static void unmodularDir(const char *path)
{
    DIR *d;
    struct dirent *dir;
    d = opendir(path);
    if (d) {
        struct dirent *entries[1000];
        int count = 0;
        while ((dir = readdir(d)) != NULL) {
            if (dir->d_type == DT_REG) {
                entries[count++] = dir;
            }
        }
        closedir(d);

        qsort(entries, count, sizeof(struct dirent*), compareToSort);

        for (int i = 0; i < count; i++) {
            dir = entries[i];
            char *dot = strrchr(dir->d_name, '.');
            if (!dot || dot == dir->d_name) continue;
            char filename[1000];
            sprintf(filename, "%s/%.*s", path, (int)(dot - dir->d_name), dir->d_name);
            FILE *outfile;
            outfile = fopen(filename, "a");
            if (outfile == NULL) {
                perror("Error opening file");
                continue;
            }
            char filepath[1000];
            sprintf(filepath, "%s/%s", path, dir->d_name);
            FILE *infile = fopen(filepath, "r");
            if (infile == NULL) {
                perror("Error opening file");
                continue;
            }
            singleDescLog("REPORT", "WRITE", filepath);
            char buffer[CHUNK_SIZE];
            size_t n;
            while ((n = fread(buffer, 1, CHUNK_SIZE, infile)) > 0) {
                fwrite(buffer, 1, n, outfile);
            }
            fclose(infile);
            fclose(outfile);

            if (remove(filepath) != 0) {
                perror("Error while deleting file");
            }
        }
    }
}

static int xmp_getattr(const char *path, struct stat *stbuf)
{
    int res;
    char fpath[1000];
    
    sprintf(fpath,"%s%s",dirpath,path);

    res = lstat(fpath, stbuf);

    if (strncmp(fpath, modPrefix, 7) == 0) {
        modularDir(fpath);
    }

    if (res == -1) return -errno;

    return 0;
}

static void checkDir(const char *path) {
    DIR *dp;
    struct dirent *de;
    struct stat st;

    dp = opendir(path);
    if (dp == NULL) return;

    while ((de = readdir(dp)) != NULL) {

        char fullPath[1256];
        sprintf(fullPath, "%s/%s", path, de->d_name);

        char folder[1000];
        sprintf(folder, "%s", de->d_name);

        if (strcmp(folder, ".") == 0 || strcmp(folder, "..") == 0)
            continue;

        if (stat(fullPath, &st) == 0 && S_ISDIR(st.st_mode)) {
            if (strncmp(folder, modPrefix, 7) == 0) {
                modularDir(fullPath);
            }
            checkDir(fullPath);
        }
    }

    closedir(dp);
}

static int xmp_readdir(const char *path, void *buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info *fi)
{
    char fpath[1000];

    if(strcmp(path,"/") == 0)
    {
        path=dirpath;
        sprintf(fpath,"%s",path);
    } else sprintf(fpath, "%s%s",dirpath,path);

    int res = 0;

    DIR *dp;
    struct dirent *de;
    (void) offset;
    (void) fi;

    dp = opendir(fpath);

    if (dp == NULL) return -errno;

    while ((de = readdir(dp)) != NULL) {
        struct stat st;

        memset(&st, 0, sizeof(st));

        st.st_ino = de->d_ino;
        st.st_mode = de->d_type << 12;

        res = (filler(buf, de->d_name, &st, 0));

        if(res!=0) break;
    }

    checkDir(fpath);

    closedir(dp);

    return 0;
}

static int xmp_read(const char *path, char *buf, size_t size, off_t offset, struct fuse_file_info *fi)
{
    char fpath[1000];

    if(strcmp(path,"/") == 0)
    {
        path=dirpath;
        sprintf(fpath,"%s",path);
    } else sprintf(fpath, "%s%s",dirpath,path);

    int res = 0;
    int fd = 0 ;

    (void) fi;

    fd = open(fpath, O_RDONLY);

    if (fd == -1) return -errno;

    res = pread(fd, buf, size, offset);

    if (res == -1) res = -errno;

    close(fd);

    return res;
}

static int xmp_unlink(const char *path)
{
    char fpath[1000];
    if(strcmp(path,"/") == 0)
    {
        path=dirpath;
        sprintf(fpath,"%s",path);
    }
    else sprintf(fpath, "%s%s",dirpath,path);

    int res;

    res = unlink(fpath);
    if (res == -1)
        return -errno;

    singleDescLog("FLAG", "UNLINK", fpath);

    return 0;
}

static int xmp_rmdir(const char *path)
{
    char fpath[1000];
    if(strcmp(path,"/") == 0)
    {
        path=dirpath;
        sprintf(fpath,"%s",path);
    }
    else sprintf(fpath, "%s%s",dirpath,path);

    int res;

    res = rmdir(fpath);
    if (res == -1)
        return -errno;

    singleDescLog("FLAG", "RMDIR", fpath);

    return 0;
}

static int xmp_open(const char *path, struct fuse_file_info *fi)
{
    char fpath[1000];
    if(strcmp(path,"/") == 0)
    {
        path=dirpath;
        sprintf(fpath,"%s",path);
    }
    else sprintf(fpath, "%s%s",dirpath,path);

    int res;

    res = open(fpath, fi->flags);
    if (res == -1)
        return -errno;

    close(res);

    singleDescLog("REPORT", "OPEN", fpath);

    return 0;
}

static int xmp_write(const char *path, const char *buf, size_t size, off_t offset, struct fuse_file_info *fi)
{
    char fpath[1000];
    if(strcmp(path,"/") == 0)
    {
        path=dirpath;
        sprintf(fpath,"%s",path);
    }
    else sprintf(fpath, "%s%s",dirpath,path);

    int fd;
    int res;

    (void) fi;
    fd = open(fpath, O_WRONLY);
    if (fd == -1)
        return -errno;

    res = pwrite(fd, buf, size, offset);
    if (res == -1)
        res = -errno;

    close(fd);

    singleDescLog("REPORT", "WRITE", fpath);

    return res;
}

static int xmp_mkdir(const char *path, mode_t mode)
{
    int res;
    char fpath[1000];

    sprintf(fpath, "%s%s", dirpath, path);

    res = mkdir(fpath, mode);

    if (res == -1)
        return -errno;

    singleDescLog("REPORT", "MKDIR", fpath);

    return 0;
}

static int xmp_rename(const char *from, const char *to)
{
	int res;
    char fromPath[1000], toPath[1000];
    char *basenameFrom = basename(strdup(from));
    char *basenameTo = basename(strdup(to));

    snprintf(fromPath, sizeof(fromPath), "%s%s", dirpath, from);
    snprintf(toPath, sizeof(toPath), "%s%s", dirpath, to);

    res = rename(fromPath, toPath);

    if (res == -1)
        return -errno;

    if (strncmp(basenameTo, modPrefix, 7) == 0) {
        modularDir(toPath);

        doubleDescLog("REPORT", "RENAME", fromPath, toPath);
    }

    if (strncmp(basenameFrom, modPrefix, 7) == 0) {
        unmodularDir(toPath);

        doubleDescLog("REPORT", "RENAME", fromPath, toPath);
    }

    return 0;
}

static struct fuse_operations xmp_oper = {
    .getattr = xmp_getattr,
    .readdir = xmp_readdir,
    .read = xmp_read,
    .mkdir = xmp_mkdir,
    .rename = xmp_rename, 
    .rmdir = xmp_rmdir,
    .unlink = xmp_unlink,
    .write = xmp_write,
    .open = xmp_open,
};

int main(int argc, char *argv[])
{
    umask(0);
    return fuse_main(argc, argv, &xmp_oper, NULL);
}