#define FUSE_USE_VERSION 30
#include <fuse.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <dirent.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <openssl/bio.h>
#include <openssl/evp.h>
#include <unistd.h>


// declare fungsi
//-----------------------------------------------------//
static int textfs_getattr(const char *path, struct stat *stbuf);
static int textfs_readdir(const char *path, void *buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info *fi);
static int textfs_read(const char *path, char *buf, size_t size, off_t offset, struct fuse_file_info *fi);

static int decode_content(const char *path, char *content, size_t size);
static void reverse_filename(char *filename);
static void delete_file(const char *path);
static int textfs_rename(const char *oldpath, const char *newpath);

static int check_access(const char *path, int mask);

static void reverse_file_content(const char *filepath);


static const char *src_folder = "/home/areuka/modul4/data";
static const char *mount_point = "/home/areuka/modul4/tes";

static struct fuse_operations textfs_operations = {
    .getattr = textfs_getattr,
    .readdir = textfs_readdir,
    .read = textfs_read,
    .rename = textfs_rename,
};
//-----------------------------------------------------//

// ini main
//-----------------------------------------------------//
int main(int argc, char *argv[])
{
    umask(0);
    return fuse_main(argc, argv, &textfs_operations, NULL);
}
//-----------------------------------------------------//

// ini buat poin folder sisop
//-----------------------------------------------------//


//-----------------------------------------------------//

// ini buat poin folder gallery
//-----------------------------------------------------//
static int textfs_rename(const char *oldpath, const char *newpath) {
    char fullpath_old[1000];
    char fullpath_new[1000];

    sprintf(fullpath_old, "%s%s", src_folder, oldpath);
    sprintf(fullpath_new, "%s%s", src_folder, newpath);

    char *file_name = strrchr(newpath, '/');
    if (file_name != NULL) {
        file_name++;

        char *folder_name = strstr(newpath, "rev-test");
        if (folder_name != NULL) {
            reverse_filename(file_name);

            // buat debug
            printf("DEBUG: Reversed filename: %s\n", file_name);
        }
    }

    if (rename(fullpath_old, fullpath_new) == -1) {
        perror("Error renaming file");
        return -errno;
    }

    return 0;
}


static void reverse_filename(char *filename) {
    size_t len = strlen(filename);

    // nyari extension
    char *dot = strrchr(filename, '.');
    
    if (dot != NULL) {
        size_t base_len = dot - filename;
        size_t i, j;
        char temp;

        for (i = 0, j = base_len - 1; i < j; ++i, --j) {
            temp = filename[i];
            filename[i] = filename[j];
            filename[j] = temp;
        }
    }
    else {
        size_t i, j;
        char temp;

        for (i = 0, j = len - 1; i < j; ++i, --j) {
            temp = filename[i];
            filename[i] = filename[j];
            filename[j] = temp;
        }
    }
}


static void delete_file(const char *path)
{
    char fullpath_src[1000];
    sprintf(fullpath_src, "%s%s", src_folder, path);

    if (remove(fullpath_src) != 0)
    {
        perror("Error deleting file");
    }
}
//-----------------------------------------------------//

// ini buat poin folder tulisan
//-----------------------------------------------------//
static int textfs_getattr(const char *path, struct stat *stbuf)
{
    int res = check_access(path, R_OK);
    if (res != 0) {
        return res;
    }
    char fullpath_src[1000];
    sprintf(fullpath_src, "%s%s", src_folder, path);

    if (lstat(fullpath_src, stbuf) == -1)
    {
        return -errno;
    }

    return 0;
}

static int textfs_readdir(const char *path, void *buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info *fi)
{
    int res = check_access(path, R_OK);
    if (res != 0) {
        return res;
    }
    DIR *dp;
    struct dirent *de;

    (void)offset;
    (void)fi;

    char fullpath_src[1000];
    sprintf(fullpath_src, "%s%s", src_folder, path);

    dp = opendir(fullpath_src);
    if (dp == NULL)
    {
        perror("Error opening directory");
        return -errno;
    }

    while ((de = readdir(dp)) != NULL)
    {
        if (filler(buf, de->d_name, NULL, 0))
        {
            break;
        }

        if (strcmp(path, "/tes/gallery/delete") == 0)
        {
            char fullpath_file[1000];
            sprintf(fullpath_file, "%s/%s", fullpath_src, de->d_name);
            delete_file(fullpath_file);
        }
    }
    closedir(dp);
    return 0;
}

static int textfs_read(const char *path, char *buf, size_t size, off_t offset, struct fuse_file_info *fi)
{
    char fullpath_src[1000];
    sprintf(fullpath_src, "%s%s", src_folder, path);

    FILE *file = fopen(fullpath_src, "r+");
    if (file == NULL)
    {
        return -errno;
    }

    fseek(file, offset, SEEK_SET);

    size_t bytesRead = fread(buf, 1, size, file);

    // cek prefix test- dalam folder sisop
    if (strncmp(path, "/sisop", strlen("/sisop")) == 0 && strncmp(buf, "test-", strlen("test-")) == 0) {
        reverse_file_content(fullpath_src);

        fseek(file, offset, SEEK_SET);
        fwrite(buf, 1, bytesRead, file);
    }

    fclose(file);

    size_t decodedSize = decode_content(path, buf, bytesRead);

    return decodedSize;
}


static int decode_content(const char *path, char *content, size_t size)
{
    if (strstr(path, "base64") != NULL)
    {
        // Base64 decoding
        BIO *b64 = BIO_new(BIO_f_base64());
        BIO *mem = BIO_new_mem_buf(content, size);
        mem = BIO_push(b64, mem);
        BIO_set_flags(mem, BIO_FLAGS_BASE64_NO_NL);
        size_t decodedSize = BIO_read(mem, content, size);
        BIO_free_all(mem);
        return decodedSize;
    }
    else if (strstr(path, "rot13") != NULL)
    {
        // ROT13 decoding
        for (size_t i = 0; i < size; ++i)
        {
            if ((content[i] >= 'a' && content[i] <= 'z'))
            {
                content[i] = (((content[i] - 'a') + 13) % 26) + 'a';
            }
            else if ((content[i] >= 'A' && content[i] <= 'Z'))
            {
                content[i] = (((content[i] - 'A') + 13) % 26) + 'A';
            }
        }
        return size;
    }
    else if (strstr(path, "hex") != NULL)
    {
        // hex decoding
        size_t j = 0;
        for (size_t i = 0; i < size; i += 2)
        {
            char byte[3] = {content[i], content[i + 1], '\0'};
            content[j++] = strtol(byte, NULL, 16);
        }
        return j;
    }
    else if (strstr(path, "rev") != NULL)
    {
        // reverse file
        size_t i, j;
        char temp;
        for (i = 0, j = size - 1; i < j; ++i, --j)
        {
            temp = content[i];
            content[i] = content[j];
            content[j] = temp;
        }
        return size;
    }
    else if (strstr(path, "test") != NULL)
    {
        // reverse file
        size_t i, j;
        char temp;
        for (i = 0, j = size - 1; i < j; ++i, --j)
        {
            temp = content[i];
            content[i] = content[j];
            content[j] = temp;
        }
        return size;
    }
    else
    {
        return size;
    }
}

//-----------------------------------------------------//

//ini poin folder disable-area
//-----------------------------------------------------//
static int check_access(const char *path, int mask) {
    if (strncmp(path, "/disable-area", strlen("/disable-area")) == 0) {
        if (mask & (X_OK | R_OK) || (mask & (X_OK | R_OK) && strchr(path, '/') == NULL)) {
            static int is_directory_opened = 0;
            if (is_directory_opened) {
                return 0;
            }

            char user_password[100];
            printf("Masukkan password: ");
            scanf("%s", user_password);

            const char *correct_password = "areuka";

            if (strcmp(user_password, correct_password) != 0) {
                printf("Password salah. Akses ditolak.\n");
                return -EACCES;
            } else {
                is_directory_opened = 1;
            }
        }
    }

    return 0;
}
//-----------------------------------------------------//
