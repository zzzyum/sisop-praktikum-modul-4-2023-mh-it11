#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <openssl/bio.h>
#include <openssl/evp.h>

#define BUFFER_SIZE 1024

void base64_decode(const char *input, char *output) {
    BIO *bio, *b64;

    b64 = BIO_new(BIO_f_base64());
    BIO_set_flags(b64, BIO_FLAGS_BASE64_NO_NL);

    bio = BIO_new_mem_buf(input, -1);
    bio = BIO_push(b64, bio);

    BIO_read(bio, output, strlen(input));
    BIO_free_all(bio);
}

int main() {
    FILE *file;
    char buffer[BUFFER_SIZE];
    char decodedBuffer[BUFFER_SIZE * 2]; 

    file = fopen("zip-pass.txt", "r");
    if (file == NULL) {
        perror("Error opening file");
        return 1;
    }

    fgets(buffer, BUFFER_SIZE, file);

    base64_decode(buffer, decodedBuffer);

    printf("Decrypted content:\n%s\n", decodedBuffer);

    fclose(file);

    return 0;
}
