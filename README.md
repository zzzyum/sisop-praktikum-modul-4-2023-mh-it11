# Kelompok IT11 Sistem Operasi

Kelompok IT11 :
| Nama | NRP |
| -------------------------------------- | ---------- |
| Mohammad Arkananta Radithya Taratugang | 5027221003 |
| Fazrul Ahmad Fadhilah | 5027221025 |
| Samuel Yuma Krismata | 5027221029 |

---

## Soal 1
Anthoni Salim merupakan seorang pengusaha yang memiliki supermarket terbesar di Indonesia. Ia sedang melakukan inovasi besar dalam dunia bisnis ritelnya. Salah satu ide cemerlang yang sedang dia kembangkan adalah terkait dengan pengelolaan foto dan gambar produk dalam sistem manajemen bisnisnya. Anthoni bersama sejumlah rekan bisnisnya sedang membahas konsep baru yang akan mengubah cara produk-produk di supermarketnya dipresentasikan dalam katalog digital. Untuk resource dapat di download pada link ini.
- Pada folder “gallery”, agar katalog produk lebih menarik dan kreatif, Anthoni dan tim memutuskan untuk:
    - Membuat folder dengan prefix "rev." Dalam folder ini, setiap gambar yang dipindahkan ke dalamnya akan mengalami pembalikan nama file-nya. <br> <br>
	        Ex: "mv EBooVNhNe7tU7q08jgTe.HEIC rev-test/" <br>
            Output: eTgj80q7Ut7eNhNVooBE.HEIC
    - Anthoni dan timnya ingin menghilangkan gambar-gambar produk yang sudah tidak lagi tersedia dengan membuat folder dengan prefix "delete." Jika sebuah gambar produk dipindahkan ke dalamnya, nama file-nya akan langsung terhapus. <br> <br>
Ex: "mv coba-deh.jpg delete-foto/"
- Pada folder "sisop," terdapat file bernama "script.sh." Anthoni dan timnya menyadari pentingnya menjaga keamanan dan integritas data dalam folder ini. 
    - Mereka harus mengubah permission pada file "script.sh" karena jika dijalankan maka dapat menghapus semua dan isi dari  "gallery," "sisop," dan "tulisan."
    - Anthoni dan timnya juga ingin menambahkan fitur baru dengan membuat file dengan prefix "test" yang ketika disimpan akan mengalami pembalikan (reverse) isi dari file tersebut.  
- Pada folder "tulisan" Anthoni ingin meningkatkan kemampuan sistem mereka dalam mengelola berkas-berkas teks dengan menggunakan fuse.
    - Jika sebuah file memiliki prefix "base64," maka sistem akan langsung mendekode isi file tersebut dengan algoritma Base64.
    - Jika sebuah file memiliki prefix "rot13," maka isi file tersebut akan langsung di-decode dengan algoritma ROT13.
    - Jika sebuah file memiliki prefix "hex," maka isi file tersebut akan langsung di-decode dari representasi heksadesimalnya.
    - Jika sebuah file memiliki prefix "rev," maka isi file tersebut akan langsung di-decode dengan cara membalikkan teksnya.
Contoh

- Pada folder “disable-area”, Anthoni dan timnya memutuskan untuk menerapkan kebijakan khusus. Mereka ingin memastikan bahwa folder dengan prefix "disable" tidak dapat diakses tanpa izin khusus. 
    - Jika seseorang ingin mengakses folder dan file pada “disable-area”, mereka harus memasukkan sebuah password terlebih dahulu (password bebas). 

- Setiap proses yang dilakukan akan tercatat pada logs-fuse.log dengan format :
<b> [SUCCESS/FAILED]::dd/mm/yyyy-hh:mm:ss::[tag]::[information] </b>
<b> Ex: </b>
[SUCCESS]::01/11/2023-10:43:43::rename::Move from /gallery/DuIJWColl2UYknZ8ubz6.HEIC to /gallery/foto/DuIJWColl2UYknZ8ubz6

### Code

```c
#define FUSE_USE_VERSION 30
#include <fuse.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <dirent.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <openssl/bio.h>
#include <openssl/evp.h>
#include <unistd.h>


// declare fungsi
//-----------------------------------------------------//
static int textfs_getattr(const char *path, struct stat *stbuf);
static int textfs_readdir(const char *path, void *buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info *fi);
static int textfs_read(const char *path, char *buf, size_t size, off_t offset, struct fuse_file_info *fi);

static int decode_content(const char *path, char *content, size_t size);
static void reverse_filename(char *filename);
static void delete_file(const char *path);
static int textfs_rename(const char *oldpath, const char *newpath);

static int check_access(const char *path, int mask);

static void reverse_file_content(const char *filepath);


static const char *src_folder = "/home/areuka/modul4/data";
static const char *mount_point = "/home/areuka/modul4/tes";

static struct fuse_operations textfs_operations = {
    .getattr = textfs_getattr,
    .readdir = textfs_readdir,
    .read = textfs_read,
    .rename = textfs_rename,
};
//-----------------------------------------------------//

// ini main
//-----------------------------------------------------//
int main(int argc, char *argv[])
{
    umask(0);
    return fuse_main(argc, argv, &textfs_operations, NULL);
}
//-----------------------------------------------------//

// ini buat poin folder sisop
//-----------------------------------------------------//


//-----------------------------------------------------//

// ini buat poin folder gallery
//-----------------------------------------------------//
static int textfs_rename(const char *oldpath, const char *newpath) {
    char fullpath_old[1000];
    char fullpath_new[1000];

    sprintf(fullpath_old, "%s%s", src_folder, oldpath);
    sprintf(fullpath_new, "%s%s", src_folder, newpath);

    char *file_name = strrchr(newpath, '/');
    if (file_name != NULL) {
        file_name++;

        char *folder_name = strstr(newpath, "rev-test");
        if (folder_name != NULL) {
            reverse_filename(file_name);

            // buat debug
            printf("DEBUG: Reversed filename: %s\n", file_name);
        }
    }

    if (rename(fullpath_old, fullpath_new) == -1) {
        perror("Error renaming file");
        return -errno;
    }

    return 0;
}


static void reverse_filename(char *filename) {
    size_t len = strlen(filename);

    // nyari extension
    char *dot = strrchr(filename, '.');
    
    if (dot != NULL) {
        size_t base_len = dot - filename;
        size_t i, j;
        char temp;

        for (i = 0, j = base_len - 1; i < j; ++i, --j) {
            temp = filename[i];
            filename[i] = filename[j];
            filename[j] = temp;
        }
    }
    else {
        size_t i, j;
        char temp;

        for (i = 0, j = len - 1; i < j; ++i, --j) {
            temp = filename[i];
            filename[i] = filename[j];
            filename[j] = temp;
        }
    }
}


static void delete_file(const char *path)
{
    char fullpath_src[1000];
    sprintf(fullpath_src, "%s%s", src_folder, path);

    if (remove(fullpath_src) != 0)
    {
        perror("Error deleting file");
    }
}
//-----------------------------------------------------//

// ini buat poin folder tulisan
//-----------------------------------------------------//
static int textfs_getattr(const char *path, struct stat *stbuf)
{
    int res = check_access(path, R_OK);
    if (res != 0) {
        return res;
    }
    char fullpath_src[1000];
    sprintf(fullpath_src, "%s%s", src_folder, path);

    if (lstat(fullpath_src, stbuf) == -1)
    {
        return -errno;
    }

    return 0;
}

static int textfs_readdir(const char *path, void *buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info *fi)
{
    int res = check_access(path, R_OK);
    if (res != 0) {
        return res;
    }
    DIR *dp;
    struct dirent *de;

    (void)offset;
    (void)fi;

    char fullpath_src[1000];
    sprintf(fullpath_src, "%s%s", src_folder, path);

    dp = opendir(fullpath_src);
    if (dp == NULL)
    {
        perror("Error opening directory");
        return -errno;
    }

    while ((de = readdir(dp)) != NULL)
    {
        if (filler(buf, de->d_name, NULL, 0))
        {
            break;
        }

        if (strcmp(path, "/tes/gallery/delete") == 0)
        {
            char fullpath_file[1000];
            sprintf(fullpath_file, "%s/%s", fullpath_src, de->d_name);
            delete_file(fullpath_file);
        }
    }
    closedir(dp);
    return 0;
}

static int textfs_read(const char *path, char *buf, size_t size, off_t offset, struct fuse_file_info *fi)
{
    char fullpath_src[1000];
    sprintf(fullpath_src, "%s%s", src_folder, path);

    FILE *file = fopen(fullpath_src, "r+");
    if (file == NULL)
    {
        return -errno;
    }

    fseek(file, offset, SEEK_SET);

    size_t bytesRead = fread(buf, 1, size, file);

    // cek prefix test- dalam folder sisop
    if (strncmp(path, "/sisop", strlen("/sisop")) == 0 && strncmp(buf, "test-", strlen("test-")) == 0) {
        reverse_file_content(fullpath_src);

        fseek(file, offset, SEEK_SET);
        fwrite(buf, 1, bytesRead, file);
    }

    fclose(file);

    size_t decodedSize = decode_content(path, buf, bytesRead);

    return decodedSize;
}


static int decode_content(const char *path, char *content, size_t size)
{
    if (strstr(path, "base64") != NULL)
    {
        // Base64 decoding
        BIO *b64 = BIO_new(BIO_f_base64());
        BIO *mem = BIO_new_mem_buf(content, size);
        mem = BIO_push(b64, mem);
        BIO_set_flags(mem, BIO_FLAGS_BASE64_NO_NL);
        size_t decodedSize = BIO_read(mem, content, size);
        BIO_free_all(mem);
        return decodedSize;
    }
    else if (strstr(path, "rot13") != NULL)
    {
        // ROT13 decoding
        for (size_t i = 0; i < size; ++i)
        {
            if ((content[i] >= 'a' && content[i] <= 'z'))
            {
                content[i] = (((content[i] - 'a') + 13) % 26) + 'a';
            }
            else if ((content[i] >= 'A' && content[i] <= 'Z'))
            {
                content[i] = (((content[i] - 'A') + 13) % 26) + 'A';
            }
        }
        return size;
    }
    else if (strstr(path, "hex") != NULL)
    {
        // hex decoding
        size_t j = 0;
        for (size_t i = 0; i < size; i += 2)
        {
            char byte[3] = {content[i], content[i + 1], '\0'};
            content[j++] = strtol(byte, NULL, 16);
        }
        return j;
    }
    else if (strstr(path, "rev") != NULL)
    {
        // reverse file
        size_t i, j;
        char temp;
        for (i = 0, j = size - 1; i < j; ++i, --j)
        {
            temp = content[i];
            content[i] = content[j];
            content[j] = temp;
        }
        return size;
    }
    else if (strstr(path, "test") != NULL)
    {
        // reverse file
        size_t i, j;
        char temp;
        for (i = 0, j = size - 1; i < j; ++i, --j)
        {
            temp = content[i];
            content[i] = content[j];
            content[j] = temp;
        }
        return size;
    }
    else
    {
        return size;
    }
}

//-----------------------------------------------------//

//ini poin folder disable-area
//-----------------------------------------------------//
static int check_access(const char *path, int mask) {
    if (strncmp(path, "/disable-area", strlen("/disable-area")) == 0) {
        if (mask & (X_OK | R_OK) || (mask & (X_OK | R_OK) && strchr(path, '/') == NULL)) {
            static int is_directory_opened = 0;
            if (is_directory_opened) {
                return 0;
            }

            char user_password[100];
            printf("Masukkan password: ");
            scanf("%s", user_password);

            const char *correct_password = "areuka";

            if (strcmp(user_password, correct_password) != 0) {
                printf("Password salah. Akses ditolak.\n");
                return -EACCES;
            } else {
                is_directory_opened = 1;
            }
        }
    }

    return 0;
}
//-----------------------------------------------------//

```

### Penjelasan

```c
#define FUSE_USE_VERSION 30
#include <fuse.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <dirent.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <openssl/bio.h>
#include <openssl/evp.h>
#include <unistd.h>
```

bagian ini adalah langkah awal dalam menyusun program FUSE yang kemungkinan melakukan operasi-operasi filesystem, manipulasi string, operasi kriptografi dengan OpenSSL, dan fungsi-fungsi sistem UNIX.

```c
static int textfs_getattr(const char *path, struct stat *stbuf);
static int textfs_readdir(const char *path, void *buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info *fi);
static int textfs_read(const char *path, char *buf, size_t size, off_t offset, struct fuse_file_info *fi);

static int decode_content(const char *path, char *content, size_t size);
static void reverse_filename(char *filename);
static void delete_file(const char *path);
static int textfs_rename(const char *oldpath, const char *newpath);

static int check_access(const char *path, int mask);

static void reverse_file_content(const char *filepath);


static const char *src_folder = "/home/areuka/modul4/data";
static const char *mount_point = "/home/areuka/modul4/tes";

static struct fuse_operations textfs_operations = {
    .getattr = textfs_getattr,
    .readdir = textfs_readdir,
    .read = textfs_read,
    .rename = textfs_rename,
};
```

Bagian ini adalah deklarasi fungsi-fungsi, variabel-variabel, dan struktur yang akan digunakan dalam implementasi FUSE filesystem. Berikut penjelasan masing-masing bagian:

#### Fungsi-fungsi FUSE:

1. **`textfs_getattr`**:
   - Fungsi ini mengembalikan atribut dari file atau direktori pada path yang diberikan. Dalam konteks filesystem FUSE, ini sering digunakan untuk mendapatkan informasi metadata tentang file atau direktori.

2. **`textfs_readdir`**:
   - Fungsi ini membaca isi direktori pada path yang diberikan dan mengisi buffer dengan nama-nama entri direktori. Digunakan untuk mengimplementasikan operasi membaca direktori.

3. **`textfs_read`**:
   - Fungsi ini membaca data dari file pada path yang diberikan. Implementasinya harus mengisi buffer `buf` dengan data dari file, dimulai dari offset tertentu.

4. **`decode_content`**:
   - Fungsi ini mungkin digunakan untuk mendekode konten file berdasarkan path tertentu. Konten file yang dibaca dapat diubah atau didekode sesuai dengan kondisi yang diinginkan.

5. **`reverse_filename`**:
   - Fungsi ini membalikkan nama file yang diberikan. Dapat digunakan untuk mengubah nama file secara khusus.

6. **`delete_file`**:
   - Fungsi ini menghapus file pada path yang diberikan.

7. **`textfs_rename`**:
   - Fungsi ini mengganti nama file atau direktori dari `oldpath` ke `newpath`.

8. **`check_access`**:
   - Fungsi ini memeriksa akses ke folder tertentu dengan menggunakan masker `mask`. Contoh: meminta penggunaan kata sandi sebelum mengakses folder tertentu.

9. **`reverse_file_content`**:
   - Fungsi ini mungkin digunakan untuk membalikkan konten dari suatu file.

#### Variabel dan Struktur:

1. **`const char *src_folder`**:
   - Variabel ini menyimpan path dari folder sumber yang akan digunakan dalam operasi FUSE.

2. **`const char *mount_point`**:
   - Variabel ini menyimpan path dari folder tujuan (mount point) untuk filesystem FUSE.

3. **`struct fuse_operations textfs_operations`**:
   - Struktur ini menyimpan fungsi-fungsi operasi FUSE yang akan diimplementasikan. Dalam hal ini, terdapat implementasi untuk `getattr`, `readdir`, `read`, dan `rename`.

```c
int main(int argc, char *argv[])
{
    umask(0);
    return fuse_main(argc, argv, &textfs_operations, NULL);
}
```
Fungsi `main` adalah titik masuk utama dari program. Dalam konteks ini, fungsi `main` digunakan untuk memulai eksekusi program FUSE.

1. **`umask(0)`**:
   - Fungsi ini mengatur umask program ke nilai yang ditentukan (dalam hal ini, 0). `umask` digunakan untuk menentukan hak akses default yang akan dibuang dari izin file yang baru dibuat. Dengan mengatur umask menjadi 0, program menentukan bahwa hak akses file baru akan diatur sesuai dengan izin yang diberikan oleh pemilik file.

2. **`return fuse_main(argc, argv, &textfs_operations, NULL)`**:
   - `fuse_main` adalah fungsi utama dari FUSE yang akan menangani eksekusi program dan mengarahkan operasi-operasi FUSE ke fungsi-fungsi yang telah didefinisikan sebelumnya.
   - `argc` dan `argv` adalah argumen baris perintah yang diteruskan ke program. 
   - `&textfs_operations` adalah pointer ke struktur `fuse_operations` yang berisi implementasi fungsi-fungsi operasi FUSE yang telah didefinisikan sebelumnya.
   - Parameter terakhir, `NULL`, digunakan untuk menyediakan data tambahan yang dapat digunakan dalam operasi FUSE (dalam hal ini, tidak ada data tambahan).

Jadi, secara keseluruhan, fungsi `main` ini mengatur umask dan kemudian memanggil `fuse_main` untuk memulai eksekusi program FUSE, dengan mengarahkan operasi-operasi FUSE ke fungsi-fungsi yang telah didefinisikan sebelumnya.

Lalu lanjut ke fungsi FUSE dan fungsi-fungsi lainnya. Berikut penjelasannya:

#### Fungsi `textfs_rename`:

```c
static int textfs_rename(const char *oldpath, const char *newpath) {
    char fullpath_old[1000];
    char fullpath_new[1000];

    sprintf(fullpath_old, "%s%s", src_folder, oldpath);
    sprintf(fullpath_new, "%s%s", src_folder, newpath);

    char *file_name = strrchr(newpath, '/');
    if (file_name != NULL) {
        file_name++;

        char *folder_name = strstr(newpath, "rev-test");
        if (folder_name != NULL) {
            reverse_filename(file_name);

            // buat debug
            printf("DEBUG: Reversed filename: %s\n", file_name);
        }
    }

    if (rename(fullpath_old, fullpath_new) == -1) {
        perror("Error renaming file");
        return -errno;
    }

    return 0;
}
```

- Fungsi ini mengimplementasikan operasi rename (mengganti nama) pada filesystem FUSE.
- Membentuk path lengkap dari `oldpath` dan `newpath` dengan menambahkan `src_folder`.
- Jika `newpath` berada dalam folder "rev-test", memanggil fungsi `reverse_filename` untuk membalikkan nama file.
- Menggunakan fungsi `rename` untuk mengubah nama file.
- Jika ada kesalahan, mencetak pesan kesalahan dan mengembalikan kode error.

#### Fungsi `reverse_filename`:

```c
static void reverse_filename(char *filename) {
    size_t len = strlen(filename);

    // nyari extension
    char *dot = strrchr(filename, '.');
    
    if (dot != NULL) {
        size_t base_len = dot - filename;
        size_t i, j;
        char temp;

        for (i = 0, j = base_len - 1; i < j; ++i, --j) {
            temp = filename[i];
            filename[i] = filename[j];
            filename[j] = temp;
        }
    }
    else {
        size_t i, j;
        char temp;

        for (i = 0, j = len - 1; i < j; ++i, --j) {
            temp = filename[i];
            filename[i] = filename[j];
            filename[j] = temp;
        }
    }
}
```

- Fungsi ini membalikkan nama file.
- Mencari extension file (jika ada) dan membalikkan karakter sebelumnya.
- Jika tidak ada extension, membalikkan seluruh nama file.

#### Fungsi `delete_file`:

```c
static void delete_file(const char *path)
{
    char fullpath_src[1000];
    sprintf(fullpath_src, "%s%s", src_folder, path);

    if (remove(fullpath_src) != 0)
    {
        perror("Error deleting file");
    }
}
```

- Fungsi ini menghapus file pada path tertentu.
- Membentuk path lengkap dari `path` dengan menambahkan `src_folder`.
- Menggunakan fungsi `remove` untuk menghapus file.
- Jika ada kesalahan, mencetak pesan kesalahan.

#### Fungsi `textfs_getattr`:

```c
static int textfs_getattr(const char *path, struct stat *stbuf)
{
    int res = check_access(path, R_OK);
    if (res != 0) {
        return res;
    }
    char fullpath_src[1000];
    sprintf(fullpath_src, "%s%s", src_folder, path);

    if (lstat(fullpath_src, stbuf) == -1)
    {
        return -errno;
    }

    return 0;
}
```

- Fungsi ini mengimplementasikan operasi `getattr` untuk mendapatkan atribut file atau direktori.
- Memeriksa akses dengan memanggil fungsi `check_access`.
- Membentuk path lengkap dari `path` dengan menambahkan `src_folder`.
- Menggunakan fungsi `lstat` untuk mendapatkan atribut file atau direktori.
- Jika ada kesalahan, mengembalikan kode error.

#### Fungsi `textfs_readdir`:

```c
static int textfs_readdir(const char *path, void *buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info *fi)
{
    int res = check_access(path, R_OK);
    if (res != 0) {
        return res;
    }
    DIR *dp;
    struct dirent *de;

    (void)offset;
    (void)fi;

    char fullpath_src[1000];
    sprintf(fullpath_src, "%s%s", src_folder, path);

    dp = opendir(fullpath_src);
    if (dp == NULL)
    {
        perror("Error opening directory");
        return -errno;
    }

    while ((de = readdir(dp)) != NULL)
    {
        if (filler(buf, de->d_name, NULL, 0))
        {
            break;
        }

        if (strcmp(path, "/tes/gallery/delete") == 0)
        {
            char fullpath_file[1000];
            sprintf(fullpath_file, "%s/%s", fullpath_src, de->d_name);
            delete_file(fullpath_file);
        }
    }
    closedir(dp);
    return 0;
}
```

- Fungsi ini mengimplementasikan operasi `readdir` untuk membaca isi direktori.
- Memeriksa akses dengan memanggil fungsi `check_access`.
- Membentuk path lengkap dari `path` dengan menambahkan `src_folder`.
- Membuka direktori menggunakan `opendir` dan membaca entri-entri direktori.
- Jika direktori adalah "/tes/gallery/delete", menghapus file-file di dalamnya menggunakan fungsi `delete_file`.
- Menggunakan `filler` untuk mengisi buffer dengan nama entri-entri direktori.

#### Fungsi `textfs_read`:

```c
static int textfs_read(const char *path, char *buf, size_t size, off_t offset, struct fuse_file_info *fi)
{
    char fullpath_src[1000];
    sprintf(fullpath_src, "%s%s", src_folder, path);

    FILE *file = fopen(fullpath_src, "r+");
    if (file == NULL)
    {
        return -errno;
    }

    fseek(file, offset, SEEK_SET);

    size_t bytesRead = fread(buf, 1, size, file);

    // cek prefix test- dalam folder sisop
    if (strncmp(path, "/sisop", strlen("/sisop")) == 0 && strncmp(buf, "test-", strlen("test-")) == 0) {
        reverse_file_content(fullpath_src);

        fseek(file, offset, SEEK_SET);
        fwrite(buf, 1, bytesRead, file);
    }

    fclose(file);

    size_t decodedSize = decode_content(path, buf, bytesRead);

    return decodedSize;
}
```

- Fungsi ini mengimplementasikan operasi `read` untuk membaca isi file.
- Membentuk path lengkap dari `path` dengan menambahkan `src_folder`.
- Membuka file menggunakan `fopen` dan membaca isi file.
- Jika path adalah "/sisop" dan isi file diawali dengan "test-", memanggil fungsi `reverse_file_content`.
- Menutup file setelah pembacaan.
- Mendekode konten file menggunakan fungsi `decode_content`.

#### Fungsi `decode_content`:

```c
static int decode_content(const char *path, char *content, size_t size)
{
    if (strstr(path, "base64") != NULL)
    {
        // Base64 decoding
        BIO *b64 = BIO_new(BIO_f_base64());
        BIO *mem = BIO_new_mem_buf(content, size);
        mem = BIO_push(b64, mem);
        BIO_set_flags(mem, BIO_FLAGS_BASE64_NO_NL);
        size_t decodedSize = BIO_read(mem, content, size);
        BIO_free_all(mem);
        return decodedSize;
    }
    else if (strstr(path, "rot13") != NULL)
    {
        // ROT13 decoding
        for (size_t i = 0; i < size; ++i)
        {
            if ((content[i] >= 'a' && content[i] <= 'z'))
            {
                content[i] = (((content[i] - 'a') + 13) % 26) + 'a';
            }
            else if ((content[i] >= 'A' && content[i] <= 'Z'))
            {
                content[i] = (((content[i] - 'A') + 13) % 26) + 'A';
            }
        }
        return size;
    }
    else if (strstr(path, "hex") != NULL)
    {
        // hex decoding
        size_t j = 0;
        for (size_t i = 0; i < size; i += 2)
        {
            char byte[3] = {content[i], content[i + 1], '\0'};
            content[j++] = strtol(byte, NULL, 16);
        }
        return j;
    }
    else if (strstr(path, "rev") != NULL)
    {
        // reverse file
        size_t i, j;
        char temp;
        for (i = 0, j = size - 1; i < j; ++i, --j)
        {
            temp = content[i];
            content[i] = content[j];
            content[j] = temp;
        }
        return size;
    }
    else if (strstr(path, "test") != NULL)
    {
        // reverse file
        size_t i, j;
        char temp;
        for (i = 0, j = size - 1; i < j; ++i, --j)
        {
            temp = content[i];
            content[i] = content[j];
            content[j] = temp;
        }
        return size;
    }
    else
    {
        return size;
    }
}
```

- Fungsi ini mendekode konten file berdasarkan path tertentu.
- Mendukung beberapa jenis dekripsi seperti Base64, ROT13, dan hex decoding.
- Jika path mengandung kata "rev", membalikkan konten file.

#### Fungsi `check_access`:

```c
static int check_access(const char *path, int mask) {
    if (strncmp(path, "/disable-area", strlen("/disable-area")) == 0) {
        if (mask & (X_OK | R_OK) || (mask & (X_OK | R_OK) && strchr(path, '/') == NULL)) {
            static int is_directory_opened = 0;
            if (is_directory_opened) {
                return 0;
            }

            char user_password[100];
            printf("Masukkan password: ");
            scanf("%s", user_password);

            const char *correct_password = "areuka";

            if (strcmp(user_password, correct_password) != 0) {
                printf("Password salah. Akses ditolak.\n");
                return -EACCES;
            } else {
                is_directory_opened = 1;
            }
        }
    }

    return 0;
}
```

- Fungsi ini memeriksa akses ke folder tertentu berdasarkan masker yang diberikan.
- Jika path adalah "/disable-area" dan memenuhi masker tertentu, meminta kata sandi.
- Jika kata sandi benar, memberikan akses.

### Output

#### Menjalankan Program
![Screenshot_2023-11-18_194948](/uploads/93a0698da6e7554e97c4eb191e8e0456/Screenshot_2023-11-18_194948.png) <br>
Cara menjalankan programnya adalah dengan memasukkan command `./hell -f tes`

#### Terminal
![Screenshot_2023-11-18_195109](/uploads/a16d7519af25e496dab27b08a3372411/Screenshot_2023-11-18_195109.png) <br>
Terminal kiri yang berada pada direktori `tes` merupakan mount dari FUSE

#### Direktori `gallery`
![Screenshot_2023-11-18_195125](/uploads/3f77cdc63e3b8f8857f4c2d1e53eb8b4/Screenshot_2023-11-18_195125.png)

![Screenshot_2023-11-18_195145](/uploads/22b49cbb83f6f44c23a25b17e94084a2/Screenshot_2023-11-18_195145.png)

*Notes*
Seharusnya file ter-rename namun karena ada kendala file tidak dapat ter-rename. Namun pada output debug yang dibuat menunjukkan bahwa kode untuk me-reverse nama file berhasil, hanya saja tidak dapat merubah nama file. <br>
![Screenshot_2023-11-18_195153](/uploads/682ea6d1f83fa3ce878109e2a79d5d34/Screenshot_2023-11-18_195153.png)

#### Direktori `sisop`
![Screenshot_2023-11-18_195425](/uploads/3371e93e408714adaf28e49f7aeaa1a2/Screenshot_2023-11-18_195425.png) <br>
Terminal kiri merupakan direktori mount FUSE sedangkan terminal kanan merupakan direktori asli/source.

#### Direktori `tulisan`
![Screenshot_2023-11-18_195325](/uploads/2d97b36e2a771cc946a5edce890ba521/Screenshot_2023-11-18_195325.png) <br>
Terminal kiri merupakan direktori mount FUSE sedangkan terminal kanan merupakan direktori asli/source.

#### Direktori `disable-area`
![Screenshot_2023-11-18_195508](/uploads/dd393578397641ffe81cb12361f876c7/Screenshot_2023-11-18_195508.png) <br>
![Screenshot_2023-11-18_195502](/uploads/644afc9b8c7a2520b5347e71fba21897/Screenshot_2023-11-18_195502.png) <br>
Program meminta password sebelum mengakses direktori `disable-area`.

## Soal 2
Manda adalah seorang mahasiswa IT, dimana ia merasa hari ini adalah hari yang menyebalkan karena sudah bertemu lagi dengan praktikum sistem operasi. Pada materi hari ini, ia mempelajari suatu hal bernama FUSE. Karena sesi lab telah selesai, kini waktunya untuk mengerjakan penugasan praktikum. Manda mendapatkan firasat jika soal modul kali ini adalah yang paling sulit dibandingkan modul lainnya, sehingga dia akan mulai mengerjakan tugas praktikum-nya. Sebelumnya, Manda mendapatkan file yang perlu didownload secara manual terlebih dahulu pada link ini. Selanjutnya, ia tinggal mengikuti langkah - langkah yang diminta untuk mengerjakan soal ini hingga akhir. Manda berharap ini menjadi modul terakhir sisop yang ia pelajari <br>
- Membuat file open-password.c untuk membaca file zip-pass.txt <br>
    - Melakukan proses dekripsi base64 terhadap file tersebut <br>
    - Lalu unzip home.zip menggunakan password hasil dekripsi <br>
- Membuat file semangat.c <br>
    - Setiap proses yang dilakukan akan tercatat pada logs-fuse.log dengan format : <br>
    [SUCCESS/FAILED]::dd/mm/yyyy-hh:mm:ss::[tag]::[information]
    Ex: <br>
    [SUCCESS]::06/11/2023-16:05:48::readdir::Read directory /sisop <br>
    - Selanjutnya untuk memudahkan pengelolaan file, Manda diminta untuk mengklasifikasikan file sesuai jenisnya: <br>
        - Melakukan unzip pada home.zip <br>
        - Membuat folder yang dapat langsung memindahkan file dengan kategori ekstensi file yang mereka tetapkan: <br>
            - documents: .pdf dan .docx. <br>
            - images: .jpg, .png, dan .ico. <br>
            - website: .js, .html, dan .json. <br>
            - sisop: .c dan .sh. <br>
            - text: .txt. <br>
            - aI: .ipynb dan .csv. <br>
    - Karena sedang belajar tentang keamanan, tiap kali mengakses file (cat nama-file) pada di dalam folder text maka perlu memasukkan password terlebih dahulu <br>
        - Password terdapat di file password.bin <br>
    - Pada folder website <br>
        - Membuat file csv pada folder website dengan format ini: file,title,body <br>
        - Tiap kali membaca file csv dengan format yang telah ditentukan, maka akan langsung tergenerate sebuah file html sejumlah yang telah dibuat pada file csv. <br>
    - Tidak dapat menghapus file / folder yang mengandung prefix “restricted” <br>
    - Pada folder documents <br>
        - Karena ingin mencatat hal - hal penting yang mungkin diperlukan, maka Manda diminta untuk menambahkan detail - detail kecil dengan memanfaatkan attribut <br>
- Membuat file server.c <br>
    - Pada folder ai, terdapat file webtoon.csv <br>
    - Manda diminta untuk membuat sebuah server (socket programming) untuk membaca webtoon.csv. Dimana terjadi pengiriman data antara client ke server dan server ke client. <br>
        - Menampilkan seluruh judul <br>
        - Menampilkan berdasarkan genre <br>
        - Menampilkan berdasarkan hari <br>
        - Menambahkan ke dalam file webtoon.csv <br>
        - Melakukan delete berdasarkan judul <br>
        - Selain command yang diberikan akan menampilkan tulisan “Invalid Command” <br>
    - Manfaatkan client.c pada folder sisop sebagai client <br>

**open-password.c**

### Code
```c
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <openssl/bio.h>
#include <openssl/evp.h>

#define BUFFER_SIZE 1024

void base64_decode(const char *input, char *output) {
    BIO *bio, *b64;

    b64 = BIO_new(BIO_f_base64());
    BIO_set_flags(b64, BIO_FLAGS_BASE64_NO_NL);

    bio = BIO_new_mem_buf(input, -1);
    bio = BIO_push(b64, bio);

    BIO_read(bio, output, strlen(input));
    BIO_free_all(bio);
}

int main() {
    FILE *file;
    char buffer[BUFFER_SIZE];
    char decodedBuffer[BUFFER_SIZE * 2]; 

    file = fopen("zip-pass.txt", "r");
    if (file == NULL) {
        perror("Error opening file");
        return 1;
    }

    fgets(buffer, BUFFER_SIZE, file);

    base64_decode(buffer, decodedBuffer);

    printf("Decrypted content:\n%s\n", decodedBuffer);

    fclose(file);

    return 0;
}
```

### Penjelasan
Fungsi base64_decode: <br>

Fungsi ini bertujuan untuk melakukan dekode base64 dari string input ke dalam string output. <br>
- Membuat objek BIO (b64) yang merupakan enkripsi base64. <br>
- Membuat objek BIO (bio) dari string input. <br>
- Menggabungkan kedua objek BIO untuk membentuk sebuah pipeline enkripsi. <br>
- Membaca data yang telah didekode ke dalam buffer output dan membersihkan objek BIO setelah selesai. <br>

Fungsi main(): <br>

- Membuka file "zip-pass.txt" dalam mode baca. <br>
- Jika file tidak dapat dibuka, program akan mencetak pesan error dan keluar. <br>
- Membaca isi file ke dalam buffer. <br>
- Memanggil fungsi base64_decode untuk mendekode isi buffer yang telah dibaca ke dalam decodedBuffer. <br>
- Mencetak hasil dekode ke layar. <br>
- Menutup file dan mengakhiri program. <br>

### Output decrypted
![output_decrypted_modul_4](/uploads/38f59b265b10d7cdf5cdcfbafd26dccb/output_decrypted_modul_4.jpg)

### Setelah mengunzip home.zip
![hasil_unzip_home.zip_moful_4](/uploads/627c2d2f0f9d605b39be9a1efef0ca19/hasil_unzip_home.zip_moful_4.jpg)

## Soal 3

Dalam kegelapan malam yang sepi, bulan purnama menerangi langit dengan cahaya peraknya. Rumah-rumah di sekitar daerah itu terlihat seperti bayangan yang menyelimuti jalan-jalan kecil yang sepi. Suasana ini terasa aneh, membuat hati seorang wanita bernama Sarah merasa tidak nyaman.<br>

Dia berjalan ke jendela kamarnya, menatap keluar ke halaman belakang. Pohon-pohon besar di halaman tampak gelap dan menyeramkan dalam sinar bulan. Sesekali, cahaya bulan yang redup itu memperlihatkan bayangan-bayangan aneh yang bergerak di antara pepohonan.<br>

Tiba-tiba, Ting! Pandangannya tertuju pada layar smartphonenya. Dia terkejut bukan kepalang. Notifikasi barusan adalah remainder pengumpulan praktikum yang dikirim oleh asisten praktikumnya. Sarah lupa bahwa 2 jam lagi adalah waktu pengumpulan tugas praktikum mata kuliah Sistem Operasi. Sarah sejenak merasa putus asa, saat dia menyadari bahwa ketidaknyamanan yang dirasakannya adalah akibat tekanan tugas praktikumnya.<br>

Sarah melihat tugas praktikum yang harus dia selesaikan, dan dalam hitungan detik, rasa panik melanda. Tugas ini tampaknya sangat kompleks, dan dia belum sepenuhnya siap. Sebagai seorang mahasiswa teknik komputer, mata kuliah Sistem Operasi adalah salah satu mata kuliah yang sangat menuntut, dan dia harus menyelesaikan tugas ini dengan baik untuk menjaga nilai akademiknya.<br>

Tugas yang diberikan adalah untuk membuat sebuah filesystem dengan ketentuan sebagai berikut: <br>

- Pada filesystem tersebut, jika User membuat atau me-rename sebuah direktori dengan awalan **module_**, maka direktori tersebut akan menjadi direktori modular.
- Jika sebuah direktori adalah direktori modular, maka akan dilakukan modularisasi pada folder tersebut dan juga sub-direktorinya.

- Sebuah file nantinya akan terbentuk bernama **fs_module.log** pada direktori home pengguna (**/home/[user]/fs_module.log**) yang berguna menyimpan daftar perintah **system call** yang telah dijalankan dengan sesuai dengan ketentuan berikut:<br>

    a.  Log akan dibagi menjadi beberapa level, yaitu **REPORT** dan **FLAG**.<br>

    b. Pada level log **FLAG**, log akan mencatat setiap kali terjadi system call **rmdir** (untuk menghapus direktori) dan **unlink** (untuk menghapus file). Sedangkan untuk sisa operasi lainnya, akan dicatat dengan level log **REPORT**.<br>

    c. Format untuk logging yaitu sebagai berikut.<br>
       **[LEVEL]::[yy][mm][dd]-[HH]:[MM]:[SS]::[CMD]::[DESC ...]**

    Contoh:<br>
    REPORT::231105-12:29:28::RENAME::/home/sarah/selfie.jpg::/home/sarah/cantik.jpg<br>
    REPORT::231105-12:29:33::CREATE::/home/sarah/test.txt<br>
    FLAG::231105-12:29:33::RMDIR::/home/sarah/folder

- Saat dilakukan modularisasi, file yang sebelumnya ada pada direktori asli akan menjadi file-file kecil sebesar 1024 bytes dan menjadi normal ketika diakses melalui filesystem yang dirancang oleh dia sendiri.<br>
**Contoh:**<br>
file **File_Sarah.txt** berukuran 3 kB pada direktori asli akan menjadi 3 file kecil, yakni **File_Sarah.txt.000**, **File_Sarah.txt.001**, dan **File_Sarah.txt.002** yang masing-masing berukuran 1 kB (1 kiloBytes atau 1024 bytes).

- Apabila sebuah direktori modular di-rename menjadi tidak modular, maka isi atau konten direktori tersebut akan menjadi utuh kembali (*fixed*).

### Code

``` c
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <dirent.h>
#include <errno.h>
#include <libgen.h>
#include <pthread.h>
#include <sys/stat.h>
#include <sys/time.h>

#define FUSE_USE_VERSION 28
#include <fuse.h>

#define CHUNK_SIZE 1024

static const char *dirpath = "/home/yumz/main";
static const char *modPrefix = "module_";

static void singleDescLog(char *level, char *cmd, char *desc)
{
    char timestamp[20];

    FILE *logFile = fopen("/home/yumz/fs_module.log", "a");

    time_t t = time(NULL);
    struct tm *tm_info = localtime(&t);

    strftime(timestamp, sizeof(timestamp), "%y%m%d-%H:%M:%S", tm_info);

    fprintf(logFile, "%s::%s::%s::%s\n", level, timestamp, cmd, desc);

    fclose(logFile);
}

static void doubleDescLog(char *level, char *cmd, char *firstDesc, char *secondDesc)
{
    char timestamp[20];

    FILE *logFile = fopen("/home/yumz/fs_module.log", "a");

    time_t t = time(NULL);
    struct tm *tm_info = localtime(&t);

    strftime(timestamp, sizeof(timestamp), "%y%m%d-%H:%M:%S", tm_info);

    fprintf(logFile, "%s::%s::%s::%s::%s\n", level, timestamp, cmd, firstDesc, secondDesc);

    fclose(logFile);
}

static void modularDir(const char *dirPath)
{
    DIR *dir = opendir(dirPath);
    if (dir == NULL) {
        perror("Error opening directory for modularization");
        return;
    }

    struct dirent *entry;
    while ((entry = readdir(dir)) != NULL) {
        if (entry->d_type == DT_REG) {
            char filePath[1024];
            sprintf(filePath, "%s/%s", dirPath, entry->d_name);

            struct stat st;
            if (stat(filePath, &st) == -1) {
                perror("Error getting file information");
                continue;
            }

            if (st.st_size > CHUNK_SIZE) {
                FILE *originalFile = fopen(filePath, "rb");
                if (originalFile == NULL) {
                    perror("Error opening file for modularization");
                    continue;
                }

                int chunkIndex = 0;
                while (!feof(originalFile)) {
                    char chunkPath[1030];
                    sprintf(chunkPath, "%s.%03d", filePath, chunkIndex);

                    FILE *chunkFile = fopen(chunkPath, "wb");
                    if (chunkFile == NULL) {
                        perror("Error creating chunk file");
                        fclose(originalFile);
                        break;
                    }

                    char buffer[CHUNK_SIZE];
                    size_t bytesRead = fread(buffer, 1, CHUNK_SIZE, originalFile);
                    fwrite(buffer, 1, bytesRead, chunkFile);

                    fclose(chunkFile);
                    chunkIndex++;
                }

                fclose(originalFile);

                if (remove(filePath) != 0) {
                    perror("Error deleting original file");
                }
            }
        }
    }

    closedir(dir);
}

static int compareToSort(const void *a, const void *b) 
{ 
    struct dirent **dirA = (struct dirent **)a;
    struct dirent **dirB = (struct dirent **)b;

    return strcmp((*dirA)->d_name, (*dirB)->d_name); 
} 

static void unmodularDir(const char *path)
{
    DIR *d;
    struct dirent *dir;
    d = opendir(path);
    if (d) {
        struct dirent *entries[1000];
        int count = 0;
        while ((dir = readdir(d)) != NULL) {
            if (dir->d_type == DT_REG) {
                entries[count++] = dir;
            }
        }
        closedir(d);

        qsort(entries, count, sizeof(struct dirent*), compareToSort);

        for (int i = 0; i < count; i++) {
            dir = entries[i];
            char *dot = strrchr(dir->d_name, '.');
            if (!dot || dot == dir->d_name) continue;
            char filename[1000];
            sprintf(filename, "%s/%.*s", path, (int)(dot - dir->d_name), dir->d_name);
            FILE *outfile;
            outfile = fopen(filename, "a");
            if (outfile == NULL) {
                perror("Error opening file");
                continue;
            }
            char filepath[1000];
            sprintf(filepath, "%s/%s", path, dir->d_name);
            FILE *infile = fopen(filepath, "r");
            if (infile == NULL) {
                perror("Error opening file");
                continue;
            }
            singleDescLog("REPORT", "WRITE", filepath);
            char buffer[CHUNK_SIZE];
            size_t n;
            while ((n = fread(buffer, 1, CHUNK_SIZE, infile)) > 0) {
                fwrite(buffer, 1, n, outfile);
            }
            fclose(infile);
            fclose(outfile);

            if (remove(filepath) != 0) {
                perror("Error while deleting file");
            }
        }
    }
}

static int xmp_getattr(const char *path, struct stat *stbuf)
{
    int res;
    char fpath[1000];
    
    sprintf(fpath,"%s%s",dirpath,path);

    res = lstat(fpath, stbuf);

    if (res == -1) return -errno;

    return 0;
}

static int xmp_readdir(const char *path, void *buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info *fi)
{
    char fpath[1000];

    if(strcmp(path,"/") == 0)
    {
        path=dirpath;
        sprintf(fpath,"%s",path);
    } else sprintf(fpath, "%s%s",dirpath,path);

    int res = 0;

    DIR *dp;
    struct dirent *de;
    (void) offset;
    (void) fi;

    dp = opendir(fpath);

    if (dp == NULL) return -errno;

    while ((de = readdir(dp)) != NULL) {
        struct stat st;

        memset(&st, 0, sizeof(st));

        st.st_ino = de->d_ino;
        st.st_mode = de->d_type << 12;
        res = (filler(buf, de->d_name, &st, 0));

        if(res!=0) break;

    }

    closedir(dp);

    return 0;
}

static int xmp_read(const char *path, char *buf, size_t size, off_t offset, struct fuse_file_info *fi)
{
    char fpath[1000];

    if(strcmp(path,"/") == 0)
    {
        path=dirpath;
        sprintf(fpath,"%s",path);
    } else sprintf(fpath, "%s%s",dirpath,path);

    int res = 0;
    int fd = 0 ;

    (void) fi;

    fd = open(fpath, O_RDONLY);

    if (fd == -1) return -errno;

    res = pread(fd, buf, size, offset);

    if (res == -1) res = -errno;

    close(fd);

    return res;
}

static int xmp_unlink(const char *path)
{
    char fpath[1000];
    if(strcmp(path,"/") == 0)
    {
        path=dirpath;
        sprintf(fpath,"%s",path);
    }
    else sprintf(fpath, "%s%s",dirpath,path);

    int res;

    res = unlink(fpath);
    if (res == -1)
        return -errno;

    singleDescLog("FLAG", "UNLINK", fpath);

    return 0;
}

static int xmp_rmdir(const char *path)
{
    char fpath[1000];
    if(strcmp(path,"/") == 0)
    {
        path=dirpath;
        sprintf(fpath,"%s",path);
    }
    else sprintf(fpath, "%s%s",dirpath,path);

    int res;

    res = rmdir(fpath);
    if (res == -1)
        return -errno;

    singleDescLog("FLAG", "RMDIR", fpath);

    return 0;
}

static int xmp_open(const char *path, struct fuse_file_info *fi)
{
    char fpath[1000];
    if(strcmp(path,"/") == 0)
    {
        path=dirpath;
        sprintf(fpath,"%s",path);
    }
    else sprintf(fpath, "%s%s",dirpath,path);

    int res;

    res = open(fpath, fi->flags);
    if (res == -1)
        return -errno;

    close(res);

    singleDescLog("REPORT", "OPEN", fpath);

    return 0;
}

static int xmp_write(const char *path, const char *buf, size_t size, off_t offset, struct fuse_file_info *fi)
{
    char fpath[1000];
    if(strcmp(path,"/") == 0)
    {
        path=dirpath;
        sprintf(fpath,"%s",path);
    }
    else sprintf(fpath, "%s%s",dirpath,path);

    int fd;
    int res;

    (void) fi;
    fd = open(fpath, O_WRONLY);
    if (fd == -1)
        return -errno;

    res = pwrite(fd, buf, size, offset);
    if (res == -1)
        res = -errno;

    close(fd);

    singleDescLog("REPORT", "WRITE", fpath);

    return res;
}

static int xmp_mkdir(const char *path, mode_t mode)
{
    int res;
    char fpath[1000];
    char *basenamePath = basename(strdup(path));

    sprintf(fpath, "%s%s", dirpath, path);

    if (strncmp(basenamePath, modPrefix, 7) == 0) {

        res = mkdir(fpath, mode);

        if (res == -1)
            return -errno;

        if (chdir(fpath) == -1) {
            perror("Error changing directory");
            return -errno;
        }

        singleDescLog("REPORT", "MKDIR", fpath);
    } else {

        res = mkdir(fpath, mode);

        if (res == -1)
            return -errno;

        singleDescLog("REPORT", "MKDIR", fpath);
    }

    return 0;
}

static int xmp_rename(const char *from, const char *to) 
{
    int res;
    char fromPath[1000], toPath[1000];
    char *basenameFrom = basename(strdup(from));
    char *basenameTo = basename(strdup(to));

    snprintf(fromPath, sizeof(fromPath), "%s%s", dirpath, from);
    snprintf(toPath, sizeof(toPath), "%s%s", dirpath, to);

    res = rename(fromPath, toPath);

    if (res == -1)
        return -errno;

    if (strncmp(basenameFrom, modPrefix, 7) == 0) {

        if (chdir(toPath) == -1) {
            perror("Error changing directory");
            return -errno;
        }

        unmodularDir(".");

        doubleDescLog("REPORT", "RENAME", fromPath, toPath);
    } else if (strncmp(basenameTo, modPrefix, 7) == 0) {
        if (chdir(toPath) == -1) {
            perror("Error changing directory");
            return -errno;
        }

        modularDir(".");

        doubleDescLog("REPORT", "RENAME", fromPath, toPath);
    } else {
        doubleDescLog("REPORT", "RENAME", fromPath, toPath);
    }

    return 0;
}

static struct fuse_operations xmp_oper = {
    .getattr = xmp_getattr,
    .readdir = xmp_readdir,
    .read = xmp_read,
    .mkdir = xmp_mkdir,
    .rename = xmp_rename, 
    .rmdir = xmp_rmdir,
    .unlink = xmp_unlink,
    .write = xmp_write,
    .open = xmp_open,
};

int main(int argc, char *argv[])
{
    umask(0);
    return fuse_main(argc, argv, &xmp_oper, NULL);
}
```

### Penjelasan

``` c
#define CHUNK_SIZE 1024

static const char *dirpath = "/home/yumz/main";
static const char *modPrefix = "module_";
```
1. Deklarasi `CHUNK_SIZE` yang merupakan ukuran maksimum dari potongan file pada fungsi yang akan dijalankan nantinya. Deklarasi variabel konstan `dirpath` yang digunakan untuk menyimpan path direktori utama yang mana path ini akan digunakan sebagai prefix untuk path-file apda operasi sistem file yang diimplementasikan. Deklarasi variabel konstan `modPretix` yang digunakan untuk menyimpan prefix untuk nama direktori modular.

``` c
static void singleDescLog(char *level, char *cmd, char *desc)
{
    char timestamp[20];

    FILE *logFile = fopen("/home/yumz/fs_module.log", "a");

    time_t t = time(NULL);
    struct tm *tm_info = localtime(&t);

    strftime(timestamp, sizeof(timestamp), "%y%m%d-%H:%M:%S", tm_info);

    fprintf(logFile, "%s::%s::%s::%s\n", level, timestamp, cmd, desc);

    fclose(logFile);
}

static void doubleDescLog(char *level, char *cmd, char *firstDesc, char *secondDesc)
{
    char timestamp[20];

    FILE *logFile = fopen("/home/yumz/fs_module.log", "a");

    time_t t = time(NULL);
    struct tm *tm_info = localtime(&t);

    strftime(timestamp, sizeof(timestamp), "%y%m%d-%H:%M:%S", tm_info);

    fprintf(logFile, "%s::%s::%s::%s::%s\n", level, timestamp, cmd, firstDesc, secondDesc);

    fclose(logFile);
}
```
2. Dibuat dua fungsi log yang berbeda hanya untuk menangani jumlah deskripsi saja yang mana fungsi `singleDescLog` untuk membuat log dengan satu deskripsi dan fungsi `doubleDescLog` untuk membuat log dengan dua deskripsi (dari fungsi `xmp_rename`).Fungsi tersebut digunakan untuk mencatat log yang terjadi dalam sistem file ke dalam file log yang mencakup beberapa informasi yaitu tingkat (level), timestamp, perintah (seperti "WRITE", "RENAME", dll) dan deskripsi atau informasi tambahan terkait peristiwa yang terjadi dalam log tersebut. Fungsi tersebut bekerja dengan cara membuka file log (`fs_moodule.log`) dalam mode append ("a") yang mana akan apabila file tersebut belum ada. Selanjutnya, fungsi akan mengambil timestamp menggunakan perintah `localtime` dan menyimpannya dalam bentuk string di dalam array `timestamp`. Kemudian, fungsi akan menulis informasi ke dalam file dengan format yang telah ditentukan dan akan menutup file log setelah menulis informasi tersebut. 

``` c
static void modularDir(const char *dirPath)
{
    DIR *dir = opendir(dirPath);
    if (dir == NULL) {
        perror("Error opening directory for modularization");
        return;
    }

    struct dirent *entry;
    while ((entry = readdir(dir)) != NULL) {
        if (entry->d_type == DT_REG) {
            char filePath[1024];
            sprintf(filePath, "%s/%s", dirPath, entry->d_name);

            struct stat st;
            if (stat(filePath, &st) == -1) {
                perror("Error getting file information");
                continue;
            }

            if (st.st_size > CHUNK_SIZE) {
                FILE *originalFile = fopen(filePath, "rb");
                if (originalFile == NULL) {
                    perror("Error opening file for modularization");
                    continue;
                }

                int chunkIndex = 0;
                while (!feof(originalFile)) {
                    char chunkPath[1030];
                    sprintf(chunkPath, "%s.%03d", filePath, chunkIndex);

                    FILE *chunkFile = fopen(chunkPath, "wb");
                    if (chunkFile == NULL) {
                        perror("Error creating chunk file");
                        fclose(originalFile);
                        break;
                    }

                    char buffer[CHUNK_SIZE];
                    size_t bytesRead = fread(buffer, 1, CHUNK_SIZE, originalFile);
                    fwrite(buffer, 1, bytesRead, chunkFile);

                    fclose(chunkFile);
                    chunkIndex++;
                }

                fclose(originalFile);

                if (remove(filePath) != 0) {
                    perror("Error deleting original file");
                }
            }
        }
    }

    closedir(dir);
}
```
3. Fungsi yang digunakan untuk melakukan proses modularisasi pada file yang terdapat dalam suatu direktori dengan cara membuka direktori, memperoses setiap file reguler yang ada di dalamnya, dan jika ukuran file melebihi batas tertentu, maka file tersebut akan dipecah menjadi file-file kecil, dan kemudian menghapus file asli. Berikut adalah penjelasan lengkapnya:
``` c
DIR *dir = opendir(dirPath)
```
- Membuka direktori yang diberikan (dirPath) dan menyimpan handle direktori dalam variabel `dir`.
``` c
if (dir == NULL) { 
    perror("Error opening directory for modularization");
    return; 
}
```
- Memeriksa apakah direktori berhasil dibuka. Jika tidak, mencetak pesan kesalahan dan mengembalikan dari fungsi.
``` c
struct dirent *entry;
while ((entry = readdir(dir)) != NULL)
```
- Membaca setiap entri (file atau direktori) dalam direktori.
``` c
if (entry->d_type == DT_REG)
```
- Memeriksa apakah entri yang sedang diproses adalah file regular (bukan direktori).
``` c
char filePath[1024];
sprintf(filePath, "%s/%s", dirPath, entry->d_name)
```
- Membentuk path lengkap file dengan menggabungkan path direktori (`dirPath`) dan nama file.
``` c
struct stat st; if (stat(filePath, &st) == -1) { perror("Error getting file information"); continue; }
```
- Mengambil informasi stat dari file untuk mendapatkan ukuran file (`st.st_size`). Jika gagal, mencetak pesan kesalahan dan melanjutkan ke file berikutnya.
``` c
if (st.st_size > CHUNK_SIZE)
``` 
- Memeriksa apakah ukuran file melebihi batas ukuran chunk (`CHUNK_SIZE`).
``` c
FILE *originalFile = fopen(filePath, "rb");
if (originalFile == NULL) { 
    perror("Error opening file for modularization"); 
    
    continue; 
}
```
- Membuka file asli dalam mode baca biner ("rb"). Jika gagal membuka, mencetak pesan kesalahan dan melanjutkan ke file berikutnya.
``` c
int chunkIndex = 0;
while (!feof(originalFile)) { 
    char chunkPath[1030]; 
    sprintf(chunkPath, "%s.%03d", filePath, chunkIndex) 
}
```
- Menginisialisasi indeks chunk dan memproses file asli ke dalam potongan-potongan kecil dan membentuk path untuk setiap chunk dengan menambahkan indeks pada nama file.
``` c
FILE *chunkFile = fopen(chunkPath, "wb");
if (chunkFile == NULL) {
    perror("Error creating chunk file");
    fclose(originalFile);
    
    break; 
}
```
- Membuka file chunk dalam mode tulis biner ("wb"). Jika gagal membuka, mencetak pesan kesalahan, menutup file asli, dan menghentikan proses chunking.
``` c
char buffer[CHUNK_SIZE];
size_t bytesRead = fread(buffer, 1, CHUNK_SIZE, originalFile);
fwrite(buffer, 1, bytesRead, chunkFile);
```
- Membaca sejumlah data dari file asli ke dalam buffer dan menulisnya ke file chunk.
``` c
fclose(chunkFile);
chunkIndex++; 
```
- Menutup file chunk setelah selesai memproses satu chunk dan meningkatkan indeks chunk.
``` c
fclose(originalFile);
if (remove(filePath) != 0) {
    perror("Error deleting original file");
}
```
- Menutup file asli setelah selesai proses chunking dan Menghapus file asli setelah file chunk selesai dibuat.
- `closedir(dir)` : Menutup direktori setelah semua file di dalamnya diproses.

**Fungsi compareToSort**
``` c
static int compareToSort(const void *a, const void *b) 
{ 
    struct dirent **dirA = (struct dirent **)a;
    struct dirent **dirB = (struct dirent **)b;

    return strcmp((*dirA)->d_name, (*dirB)->d_name); 
} 
```
4. Fungsi perbandingan yang digunakan dalam fungsi `unmodularDir` dalam proses pengurutan file dalam suatu direktori. Fungsi ini digunakan sebagai argumen perbandingan oleh fungsi `qsort` dalam fungsi `unmodularDir` untuk mengurutkan array dari entri direktori berdasarkan nama filenya. Berikut adalah penjelasan lengkapnya:
``` c
struct dirent **dirA = (struct dirent **)a;
struct dirent **dirB = (struct dirent **)b;
```
- Mengkonversi parameter `a` dan `b` (yang awalnya berupa pointer ke elemen-elemen dalam array yang akan diurutkan) ke tipe data `struct dirent **`, karena `qsort` bekerja dengan elemen-elemen yang diurutkan dalam bentuk array pointer.
``` c
return strcmp((*dirA)->d_name, (*dirB)->d_name)
```
- Membandingkan dua nama file (`d_name`) dari dua entri direktori yang diwakili oleh `dirA` dan `dirB`, menggunakan fungsi `strcmp` untuk membandingkan dua string, serta mengembalikan nilai negatif jika nama file pada `dirA` kurang dari nama file pada `dirB`, 0 jika kedua nama file sama, dan nilai positif jika nama file pada `dirA` lebih besar dari nama file pada `dirB`.

**Fungsi unmodularDir**

``` c
static void unmodularDir(const char *path)
{
    DIR *d;
    struct dirent *dir;
    d = opendir(path);
    if (d) {
        struct dirent *entries[1000];
        int count = 0;
        while ((dir = readdir(d)) != NULL) {
            if (dir->d_type == DT_REG) {
                entries[count++] = dir;
            }
        }
        closedir(d);

        qsort(entries, count, sizeof(struct dirent*), compareToSort);

        for (int i = 0; i < count; i++) {
            dir = entries[i];
            char *dot = strrchr(dir->d_name, '.');
            if (!dot || dot == dir->d_name) continue;
            char filename[1000];
            sprintf(filename, "%s/%.*s", path, (int)(dot - dir->d_name), dir->d_name);
            FILE *outfile;
            outfile = fopen(filename, "a");
            if (outfile == NULL) {
                perror("Error opening file");
                continue;
            }
            char filepath[1000];
            sprintf(filepath, "%s/%s", path, dir->d_name);
            FILE *infile = fopen(filepath, "r");
            if (infile == NULL) {
                perror("Error opening file");
                continue;
            }
            singleDescLog("REPORT", "WRITE", filepath);
            char buffer[CHUNK_SIZE];
            size_t n;
            while ((n = fread(buffer, 1, CHUNK_SIZE, infile)) > 0) {
                fwrite(buffer, 1, n, outfile);
            }
            fclose(infile);
            fclose(outfile);

            if (remove(filepath) != 0) {
                perror("Error while deleting file");
            }
        }
    }
}
```
5. Fungsi yang digunakan untuk menggabungkan kemabli potongan-potongan file hasil modularisasi menjadi file asli dalam sebuah direktori.

``` c
DIR *d; struct dirent *dir;
d = opendir(path);
if (d) {
```
- Membuka direktori yang diberikan (`path`) dan menyimpan handle direktori dalam variabel `d` dan memeriksa apakah direktori behsail dibuka sebelum melanjutkan ke langkah berikutnya.

``` c
struct dirent *entries[1000];
int count = 0;
while ((dir = readdir(d)) != NULL) {
    if (dir->d_type == DT_REG) {
        entries[count++] = dir;
    }
}
closedir(d);
```
- Membaca setiap entri (file atau direktori) dalam direktori dan menyimpan entri-entri file reguler dalam array `array`, menghitung jumlah file reguler yang ada dalam direktori, serta menutup handle direktori setelkah selesai membaca entri.

``` c
qsort(entries, count, sizeof(struct dirent*), compareToSort);
```
- Menggunakan perintah `qsort` untuk mengurutkan array `entries` berdasarkan nama file dengan menggunakan fungsi perbandingan `compareToSort`.

``` c
for (int i = 0; i < count; i++) {
    dir = entries[i];
    char *dot = strrchr(dir->d_name, '.');
    if (!dot || dot == dir->d_name) continue;
```
- Melakukan iterasi melalui entri yang telah diurutkan, mengambil satu entri pada setiap iterasi, memeriksa apakah nama file memiliki ekestensi (titik '.' dalam nama), dan jika tidak memiliki ekstensi, maka melanjutkan ke file berikutnya.

``` c
char filename[1000];
sprintf(filename, "%s/%.*s", path, (int)(dot - dir->d_name), dir->d_name);
```
- Membentuk path untuk file asli dengan menghilangkan ekstensi dari nama file modular.

``` c
FILE *outfile; outfile = fopen(filename, "a");
if (outfile == NULL) {
    perror("Error opening file"); 
    continue;
}
```
- Membuka file asli dalam mode append ("a"), yang mana apabila gagal membuka, maka pesan error akan ditampilkan dan akan dilanjutkan ke file berikutnya.

``` c
char filepath[1000];
sprintf(filepath, "%s/%s", path, dir->d_name);

FILE *infile = fopen(filepath, "r");

if (infile == NULL) {
    perror("Error opening file");
    continue;
}
```
- Membentuk path untuk file modular, membuka file modular dalam mode read ("r"), dan jika gagal membuka file tersebut, maka pesan error akan ditampilkan dan akan dilanjutkan ke file berikutnya.

``` c
singleDescLog("REPORT", "WRITE", filepath);

char buffer[CHUNK_SIZE]; size_t n;

while ((n = fread(buffer, 1, CHUNK_SIZE, infile)) > 0) {
    fwrite(buffer, 1, n, outfile);
}

fclose(infile); fclose(outfile);
```
- Mencatat operasi WRITE ke dalam log dengan menggunakan fungsi `singleDescLog`, membaca data dari file modular (`infile`) dalam potongan-potongan kecil menggunakan buffer dan menuliskannya ke file asli (`outfile`), serta menutup file modular dan file asli setelah selesai.

``` c
if (remove(filepath) != 0) {
    perror("Error while deleting file");
}
```
- Menghapus file hasil modularisasi setelah berhasil menggabungkannya ke file asli yang mana apabila gagal dihapus, maka pesan error akan ditampilkan.

**Fungsi xmp_getattr**

``` c
static int xmp_getattr(const char *path, struct stat *stbuf)
{
    int res;
    char fpath[1000];
    
    sprintf(fpath,"%s%s",dirpath,path);

    res = lstat(fpath, stbuf);

    if (res == -1) return -errno;

    return 0;
}
```
6. Fungsi yang digunakan untuk mendapatkan atribut dari sebuah file atau direktori, seperti ukuran, tanggal modifikasi, dan hak akses.

``` c
static int xmp_readdir(const char *path, void *buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info *fi)
{
    char fpath[1000];

    if(strcmp(path,"/") == 0)
    {
        path=dirpath;
        sprintf(fpath,"%s",path);
    } else sprintf(fpath, "%s%s",dirpath,path);

    int res = 0;

    DIR *dp;
    struct dirent *de;
    (void) offset;
    (void) fi;

    dp = opendir(fpath);

    if (dp == NULL) return -errno;

    while ((de = readdir(dp)) != NULL) {
        struct stat st;

        memset(&st, 0, sizeof(st));

        st.st_ino = de->d_ino;
        st.st_mode = de->d_type << 12;
        res = (filler(buf, de->d_name, &st, 0));

        if(res!=0) break;

    }

    closedir(dp);

    return 0;
}
```
7. Fungsi yang digunakan untuk membaca isi dari sebuah direktori yang mana akan mencamtukan semua file dan subdirektori dalam direktori yang ditemukan

``` c
static int xmp_read(const char *path, char *buf, size_t size, off_t offset, struct fuse_file_info *fi)
{
    char fpath[1000];

    if(strcmp(path,"/") == 0)
    {
        path=dirpath;
        sprintf(fpath,"%s",path);
    } else sprintf(fpath, "%s%s",dirpath,path);

    int res = 0;
    int fd = 0 ;

    (void) fi;

    fd = open(fpath, O_RDONLY);

    if (fd == -1) return -errno;

    res = pread(fd, buf, size, offset);

    if (res == -1) res = -errno;

    close(fd);

    return res;
}
```
8. Fungsi yang digunakan untuk membaca data dari sebuah file.

``` c
static int xmp_unlink(const char *path)
{
    char fpath[1000];
    if(strcmp(path,"/") == 0)
    {
        path=dirpath;
        sprintf(fpath,"%s",path);
    }
    else sprintf(fpath, "%s%s",dirpath,path);

    int res;

    res = unlink(fpath);
    if (res == -1)
        return -errno;

    singleDescLog("FLAG", "UNLINK", fpath);

    return 0;
}
```
9. Fungsi yang digunakan untuk menghapus file yang mana operasi penghapusan file ini akan dicatat ke dalam file log dengan menggunakan fungsi `singleDescLog` dengan informasi yang dicatat meliputi tingkat log "FLAG", perintah "UNLINK" dan path file yang dihapus.

``` c
static int xmp_rmdir(const char *path)
{
    char fpath[1000];
    if(strcmp(path,"/") == 0)
    {
        path=dirpath;
        sprintf(fpath,"%s",path);
    }
    else sprintf(fpath, "%s%s",dirpath,path);

    int res;

    res = rmdir(fpath);
    if (res == -1)
        return -errno;

    singleDescLog("FLAG", "RMDIR", fpath);

    return 0;
}
```
10. Fungsi yang digunakan untuk menghapus direktori yang mana operasi penghapusan direktori ini akan dicatat ke dalam file log dengan menggunakan fungsi `singleDescLog` dengan informasi yang dicatat meliputi tingkat log "FLAG", perintah "RMDIR" dan path direktori yang dihapus.

``` c
static int xmp_open(const char *path, struct fuse_file_info *fi)
{
    char fpath[1000];
    if(strcmp(path,"/") == 0)
    {
        path=dirpath;
        sprintf(fpath,"%s",path);
    }
    else sprintf(fpath, "%s%s",dirpath,path);

    int res;

    res = open(fpath, fi->flags);
    if (res == -1)
        return -errno;

    close(res);

    singleDescLog("REPORT", "OPEN", fpath);

    return 0;
}
```
11. Fungsi yang digunakan untuk membuka file yang mana operasi pembukaan file tersebut akan dicatat ke dalam file log dengan menggunakan fungsi `singleDescLog` dengan informasi yang dicatat meliputi tingkat log "REPORT", perintah "OPEN" dan path file yang dibuka.

``` c
static int xmp_write(const char *path, const char *buf, size_t size, off_t offset, struct fuse_file_info *fi)
{
    char fpath[1000];
    if(strcmp(path,"/") == 0)
    {
        path=dirpath;
        sprintf(fpath,"%s",path);
    }
    else sprintf(fpath, "%s%s",dirpath,path);

    int fd;
    int res;

    (void) fi;
    fd = open(fpath, O_WRONLY);
    if (fd == -1)
        return -errno;

    res = pwrite(fd, buf, size, offset);
    if (res == -1)
        res = -errno;

    close(fd);

    singleDescLog("REPORT", "WRITE", fpath);

    return res;
}
```
12. Fungsi yang digunakan untuk menulis data ke dalam sebuah file yang mana operasi penulisan tersebut akan dicatat ke dalam file log dengan menggunakan fungsi `singleDescLog` dengan informasi yang dicatat meliputi tingkat log "REPORT", perintah "WRITE", dan path file yang dilakukan operasi write ini.

``` c
static int xmp_mkdir(const char *path, mode_t mode)
{
    int res;
    char fpath[1000];
    char *basenamePath = basename(strdup(path));

    sprintf(fpath, "%s%s", dirpath, path);

    if (strncmp(basenamePath, modPrefix, 7) == 0) {

        res = mkdir(fpath, mode);

        if (res == -1)
            return -errno;

        if (chdir(fpath) == -1) {
            perror("Error changing directory");
            return -errno;
        }

        singleDescLog("REPORT", "MKDIR", fpath);
    } else {

        res = mkdir(fpath, mode);

        if (res == -1)
            return -errno;

        singleDescLog("REPORT", "MKDIR", fpath);
    }

    return 0;
}
```
13. Fungsi yang digunakan untuk membuat sebuah direktori baru yang mana dalam operasi pembuatan direktori tersebut akan dicatat ke dalam file log dengan menggunakan fungsi `singleDescLog` dengan informasi yang dicatat meliputi tingkat log "REPORT", perintah "MKDIR", dan path folder yang dibuat. Di dalam fungsi tersebut, terdapat sebuah kondisi yang mengecek apakah nama direktori (tanpa path) diawali dengan prefiks "module_" sepanjang 7 karakter. Jika ya, maka direktori dianggap sebagai direktori modular dan fungsi tersebut akan mengganti direktori kerja ke direktori yang baru dibuat tersebut dengan menggunakan fungsi chdir lalu mencatat log dari operasi tersebut. Jika tidak diawali dengan prefiks "module_", maka fungsi tersebut hanya akan membuat folder biasa dan mencatat operasinya ke dalam log.

``` c
static int xmp_rename(const char *from, const char *to) 
{
    int res;
    char fromPath[1000], toPath[1000];
    char *basenameFrom = basename(strdup(from));
    char *basenameTo = basename(strdup(to));

    snprintf(fromPath, sizeof(fromPath), "%s%s", dirpath, from);
    snprintf(toPath, sizeof(toPath), "%s%s", dirpath, to);

    res = rename(fromPath, toPath);

    if (res == -1)
        return -errno;

    if (strncmp(basenameFrom, modPrefix, 7) == 0) {

        if (chdir(toPath) == -1) {
            perror("Error changing directory");
            return -errno;
        }

        unmodularDir(".");

        doubleDescLog("REPORT", "RENAME", fromPath, toPath);
    } else if (strncmp(basenameTo, modPrefix, 7) == 0) {
        if (chdir(toPath) == -1) {
            perror("Error changing directory");
            return -errno;
        }

        modularDir(".");

        doubleDescLog("REPORT", "RENAME", fromPath, toPath);
    } else {
        doubleDescLog("REPORT", "RENAME", fromPath, toPath);
    }

    return 0;
}
```
14. Fungsi yang digunakan untuk merename sebuah direktori yang aman dalam operasi rename tersebut akan dicatat ke dalam file log dengan menggunakan fungsi `doubleDescLog` dengan informasi yang dicatat meliputi tingkat log "REPORT", perintah "RENAME", nama direktori beserta pathnya sebelum diubah dan nama direktori beserta pathnya setelah diubah. Di dalam fungsi tersebut, terdapat kondisi yang mengecek apakah nama direktori sebelum diubah diawali dengan prefiks "module_" yang mana jika iya, maka direktori yang diubah tersebut menjadi direktori biasa (bukan modular) dan fungsi `unmodularDir` akan dijalankan dan kemudian operasi tersebut akan dicatat ke dalam log. Apabila nama direktorri setelah diubah diawali dengan prefiks "module_", maka direktori yang diubah tersebut menjadi direktori modular, dan fungsi `modularDir` akan dijalankan dan kemudian operasi tersebut akan dicatat ke dalam log.

``` c
static struct fuse_operations xmp_oper = {
    .getattr = xmp_getattr,
    .readdir = xmp_readdir,
    .read = xmp_read,
    .mkdir = xmp_mkdir,
    .rename = xmp_rename, 
    .rmdir = xmp_rmdir,
    .unlink = xmp_unlink,
    .write = xmp_write,
    .open = xmp_open,
};
```
15. Mendefinisikan strutur `fuse_operations` yang menyediakan implementasi untuk berbagai operasi sistem file pada FUSE yang mana setiap anggota dari struktur ini dihubungkan dengan fungsi yang telah diimplementasikan dalam program.

``` c
int main(int argc, char *argv[])
{
    umask(0);
    return fuse_main(argc, argv, &xmp_oper, NULL);
}
```
16. Fungsi inti yang digunakan untuk mengatur hak akses default dengan nilai `0` yang artinya tidak ada perubahan pada hak akses default. Fungsi ini juga menginisialisasi FUSE dan memulai loop utama untuk menangani operasi sistem file.

### Output

Sebelum program dijalankan:

![sebelum](/uploads/3c6d6d586ae93ba7767bb0c2fa2ec193/sebelum.png)

Sesudah program dijalankan:

![sesudah](/uploads/e17a823d9def37fa8eb3aa108421799d/sesudah.png)

Sesudah rename terhadap direktori dilakukan:
- `module_test` menjadi `testonly`
- `testing` menjadi `module_sub`

![sesudah_rename](/uploads/dfb3e505f197529a5f8eb1bcbed13d0e/sesudah_rename.png)

### Revisi

Terdapat beberapa revisi pada nomor 3 yang meliputi:

``` c
static int xmp_getattr(const char *path, struct stat *stbuf)
{
    int res;
    char fpath[1000];
    
    sprintf(fpath,"%s%s",dirpath,path);

    res = lstat(fpath, stbuf);

    if (strncmp(fpath, modPrefix, 7) == 0) {
        modularDir(fpath);
    }

    if (res == -1) return -errno;

    return 0;
}
```
1. Menambahkan kondisi yang berfungsi untuk mengecek apakah sebuah direktori diawali dengan prefix ("module_") yang mana jika iya, maka fungsi `modularDir` akan dijalankan.

``` c
static void checkDir(const char *path) {
    DIR *dp;
    struct dirent *de;
    struct stat st;

    dp = opendir(path);
    if (dp == NULL) return;

    while ((de = readdir(dp)) != NULL) {

        char fullPath[1256];
        sprintf(fullPath, "%s/%s", path, de->d_name);

        char folder[1000];
        sprintf(folder, "%s", de->d_name);

        if (strcmp(folder, ".") == 0 || strcmp(folder, "..") == 0)
            continue;

        if (stat(fullPath, &st) == 0 && S_ISDIR(st.st_mode)) {
            if (strncmp(folder, modPrefix, 7) == 0) {
                modularDir(fullPath);
            }
            checkDir(fullPath);
        }
    }

    closedir(dp);
}
```
2. Menambahkan sebuah fungsi yang digunakan untuk mengecek setiap direktori ataupun sub-direktori secara rekursif (berulang kali).

``` c
static int xmp_readdir(const char *path, void *buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info *fi)
{
    char fpath[1000];

    if(strcmp(path,"/") == 0)
    {
        path=dirpath;
        sprintf(fpath,"%s",path);
    } else sprintf(fpath, "%s%s",dirpath,path);

    int res = 0;

    DIR *dp;
    struct dirent *de;
    (void) offset;
    (void) fi;

    dp = opendir(fpath);

    if (dp == NULL) return -errno;

    while ((de = readdir(dp)) != NULL) {
        struct stat st;

        memset(&st, 0, sizeof(st));

        st.st_ino = de->d_ino;
        st.st_mode = de->d_type << 12;

        res = (filler(buf, de->d_name, &st, 0));

        if(res!=0) break;
    }

    checkDir(fpath);

    closedir(dp);

    return 0;
}
```
3. Ditambahkan perintah untuk memanggil fungsi `checkDir`.

``` c
static int xmp_mkdir(const char *path, mode_t mode)
{
    int res;
    char fpath[1000];

    sprintf(fpath, "%s%s", dirpath, path);

    res = mkdir(fpath, mode);

    if (res == -1)
        return -errno;

    singleDescLog("REPORT", "MKDIR", fpath);

    return 0;
}
```
4. Menghapus percabangan `if-else`.

``` c
static int xmp_rename(const char *from, const char *to)
{
	int res;
    char fromPath[1000], toPath[1000];
    char *basenameFrom = basename(strdup(from));
    char *basenameTo = basename(strdup(to));

    snprintf(fromPath, sizeof(fromPath), "%s%s", dirpath, from);
    snprintf(toPath, sizeof(toPath), "%s%s", dirpath, to);

    res = rename(fromPath, toPath);

    if (res == -1)
        return -errno;

    if (strncmp(basenameTo, modPrefix, 7) == 0) {
        modularDir(toPath);

        doubleDescLog("REPORT", "RENAME", fromPath, toPath);
    }

    if (strncmp(basenameFrom, modPrefix, 7) == 0) {
        unmodularDir(toPath);

        doubleDescLog("REPORT", "RENAME", fromPath, toPath);
    }

    return 0;
}
```
5. Menghilangkan perintah `chdir` pada setiap kondisi `if-else` yaitu untuk memindahkan direktori kerja sekarang.

### Kendala

Beberapa kendala yang terjadi saat program nomor 3 dijalankan, yaitu:

![kendala](/uploads/b84a7c0e052004a7d5ce1b5b5c04a51a/kendala.png)

1. Tidak dapat membuat file baru dengan pesan error: `[ Error writing lock file ./[nama file]: Function not implemented ]`

